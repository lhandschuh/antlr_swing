grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat )+ EOF!;

stat
    : //expr NL -> expr
    logicExpr NL -> logicExpr

//    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST logicExpr NL -> ^(PODST ID logicExpr)
    | PRINT logicExpr NL -> ^(PRINT logicExpr)
    | NL ->
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      | POW^ atom
      | MOD^ atom
      )*
    ;
    
logicExpr
    : expr
      ( LT^ expr
      | LET^ expr
      | MT^ expr
      | MET^ expr
      | EQ^ expr
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;
    
PRINT : 'print';

VAR :'var';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
	
POW
  : '^'
  ;
  
MOD
  : '%'
  ;
  
LT
  : '<'
  ;
  
LET
  : '<='
  ;

MT
  : '>'
  ;

MET
  : '>='
  ;
  
EQ
  : '=='
  ;
