tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (e=expr
          |print
          |declare
          )*
        ;

print   : ^(PRINT e=expr) {drukuj($e.out.toString());}
        ;
        
declare : ^(VAR e=ID) {declareSymbol($e.text);}
        ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(MOD   e1=expr e2=expr) {$out = $e1.out \% $e2.out;}
        | ^(POW   e1=expr e2=expr) {$out = poteguj($e1.out, $e2.out);}
        | ^(PODST i1=ID   e2=expr) {$out = setSymbol($i1.text, $e2.out);}
        | ^(MT    e1=expr e2=expr) {$out = $e1.out > $e2.out ? 1:0;}
        | ^(LT    e1=expr e2=expr) {$out = $e1.out < $e2.out ? 1:0;}
        | ^(LET   e1=expr e2=expr) {$out = $e1.out <= $e2.out ? 1:0;}
        | ^(MET   e1=expr e2=expr) {$out = $e1.out >= $e2.out ? 1:0;}
        | ^(EQ    e1=expr e2=expr) {$out = $e1.out == $e2.out ? 1:0;}
        | INT                      {$out = getInt($INT.text);}
        | ID                       {$out = getSymbol($ID.text);}
        ;
