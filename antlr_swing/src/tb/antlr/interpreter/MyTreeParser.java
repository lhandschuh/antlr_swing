package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {
	
	protected GlobalSymbols symbols = new GlobalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }
    
    protected void declareSymbol(String var) {
    	symbols.newSymbol(var);
    }
    
    protected Integer setSymbol(String var, Integer value) {
    	symbols.setSymbol(var, value);
    	return value;
    }
    
    protected Integer getSymbol(String var) {
    	return symbols.getSymbol(var);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }
    
    protected Integer poteguj(Integer number, Integer power) {
    	Integer value = 1;
    	for(int i=0; i<power;i++) {
    		value*=number;
    	}
    	return value;
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
}
